package com.wes.processdb.unit.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.wes.processdb.company.entity.Company;
import com.wes.processdb.utilityuse.dto.UtilityDTO;
import com.wes.processdb.utilityuse.entity.Utility;
import com.wes.processdb.utilityuse.repository.UtilityRepository;
import com.wes.processdb.utilityuse.service.UtilityServiceImpl;

import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class UtilityServiceTests {

    @Mock
    private UtilityRepository utilityRepository;

    @InjectMocks
    private UtilityServiceImpl utilityService;

    private Utility utility1;
    private Utility utility2;
    private UtilityDTO utilityDTO1;
    private UtilityDTO utilityDTO2;

    @BeforeEach
    void initMocks() {
        MockitoAnnotations.openMocks(this);

        utility1 = new Utility("Test");
        utility2 = new Utility("Test2");

        utilityDTO1 = new UtilityDTO("DTO Test1");
        utilityDTO2 = new UtilityDTO("DTO Test2");

    }

    @Test
    @DisplayName("Saving utility")
    public void testCreateUtility() {
        when(utilityRepository.save(utility1)).thenReturn(utility1);

        // when
        Utility newUtility = utilityService.saveUtility(utility1);

        // then
        assertEquals(utility1, newUtility);
    }

    @Test
    @DisplayName("Saving utility DTO")
    public void testCreateUtilityDTO() {
        // Mocking the repository to return a new Utility object
        when(utilityRepository.save(any(Utility.class))).thenAnswer(invocation -> invocation.getArgument(0));

        // when
        UtilityDTO newUtilityDTO = utilityService.saveUtility(utilityDTO1);

        // then
        assertEquals(utilityDTO1.getName(), newUtilityDTO.getName());
    }

    @Test
    @DisplayName("Get utility")
    public void testGetUtility() {
        // arrange
        when(utilityRepository.findById(1L)).thenReturn(java.util.Optional.of(utility1));

        // act
        Utility retrievedUtility = utilityService.getUtility(1L);

        // assert
        assertEquals(utility1, retrievedUtility);
    }

    @Test
    @DisplayName("Get utility DTO")
    public void testGetUtilityDTO() {
        // arrange
        when(utilityRepository.findById(1L)).thenReturn(java.util.Optional.of(utility1));

        // act
        UtilityDTO retrievedUtilityDTO = utilityService.getUtilityDTO(1L);

        // assert
        assertEquals(utility1.getName(), retrievedUtilityDTO.getName());
    }


    @Test
    @DisplayName("Update utility")
    public void testUpdateUtility() {
        // arrange
        when(utilityRepository.save(any(Utility.class))).thenAnswer(invocation -> invocation.getArgument(0));
        when(utilityRepository.findById(1L)).thenReturn(java.util.Optional.of(utility1));


        // act
        Utility updateUtility = new Utility("UpdatedName");
        utilityService.updateUtility(1L, updateUtility);

        // assert
        assertEquals(utility1.getName(), "UpdatedName");
    }

    @Test
    @DisplayName("Update utility DTO")
    public void testUpdateUtilityDTO() {
        // arrange
        when(utilityRepository.findById(1L)).thenReturn(java.util.Optional.of(utility1));
        when(utilityRepository.save(any(Utility.class))).thenAnswer(invocation -> invocation.getArgument(0));

        // act
        UtilityDTO updateUtilityDTO = new UtilityDTO("UpdatedName");
        UtilityDTO updatedUtilityDTO  = utilityService.updateUtility(1L, updateUtilityDTO);

        // assert
        assertEquals(updatedUtilityDTO.getName(), "UpdatedName");
    }


    @Test
    @DisplayName("Delete utility")
    public void testDeleteUtility() {

        // act
        utilityService.deleteUtility(1L);

        // assert
        verify(utilityRepository, times(1)).deleteById(1L);
    }

    @Test
    @DisplayName("Get all utilities DTO")
    public void testGetAllUtilitiesDTO() {
        // arrange
        when(utilityRepository.findAll()).thenReturn(java.util.List.of(utility1, utility2));

        // act
        List<UtilityDTO> utilities = utilityService.getAllUtilitiesDTO();

        // assert
        assertEquals(2, utilities.size());
    }


    @Test
    @DisplayName("Get all utilities")
    public void testGetAllUtilities() {
        // arrange
        when(utilityRepository.findAll()).thenReturn(java.util.List.of(utility1, utility2));

        // act
        List<Utility> utilities = utilityService.getAllUtilities();

        // assert
        assertEquals(2, utilities.size());
    }

}