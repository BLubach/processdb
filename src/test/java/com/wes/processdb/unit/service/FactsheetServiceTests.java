package com.wes.processdb.unit.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import com.wes.processdb.factsheets.dto.FactsheetCreateRequest;
import com.wes.processdb.factsheets.dto.FactsheetResponse;
import com.wes.processdb.factsheets.entity.Factsheet;
import com.wes.processdb.factsheets.entity.Factsheetsaving;
import com.wes.processdb.factsheets.mapper.FactsheetMapper;
import com.wes.processdb.factsheets.repository.FactsheetRepository;
import com.wes.processdb.factsheets.repository.FactsheetsavingRepository;
import com.wes.processdb.factsheets.service.FactsheetServiceImpl;
import com.wes.processdb.factsheets.service.FactsheetsavingServiceImpl;
import com.wes.processdb.exception.DuplicateFieldException;
import com.wes.processdb.exception.ResourceNotFoundException;
import com.wes.processdb.company.entity.Company;
import com.wes.processdb.company.repository.CompanyRepository;

import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Optional;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(MockitoExtension.class)
public class FactsheetServiceTests {

    @Mock
    private FactsheetRepository factsheetRepository;

    @Mock
    FactsheetsavingRepository factsheetsavingRepository;

    @Mock
    private CompanyRepository companyRepository;

    @Mock
    private FactsheetMapper factsheetMapper;

    @Mock
    private FactsheetsavingServiceImpl factsheetsavingService;

    @InjectMocks
    private FactsheetServiceImpl factsheetService;

    private Factsheet factsheet1;
    private Factsheet factsheet2;

    private FactsheetCreateRequest factsheetDTO1;
    private FactsheetResponse factsheetDTO2;
    private List<Factsheet> factsheets;

    private Company company1;
    private Company company2;

    private Factsheetsaving factsheetsaving1;
    private Factsheetsaving factsheetsaving2;

    @BeforeEach
    void initMocks() {
        MockitoAnnotations.openMocks(this);

        company1 = new Company("Testcompany");
        company2 = new Company("Testcompany2");

        factsheet1 = new Factsheet(
                "Factsheet 1",
                "Current Situation 1",
                "Room For Improvement 1",
                "Measure 1",
                "Risks 1",
                "Remarks 1",
                55.0f,
                "Complexity 1",
                true,
                "Project Dependency Comment 1",
                true,
                company1);

        factsheet2 = new Factsheet(
                "Factsheet 2",
                "Current Situation 2",
                "Room For Improvement 2",
                "Measure 2",
                "Risks 2",
                "Remarks 2",
                34.0f,
                "Complexity 2",
                true,
                "Project Dependency Comment 2",
                true,
                company2);

        factsheets = Arrays.asList(factsheet1, factsheet2);
        factsheetDTO1 = new FactsheetCreateRequest(
                "Factsheet 1",
                "Current Situation 1",
                "Room For Improvement 1",
                "Measure 1",
                "Risks 1",
                "Remarks 1",
                55.0f,
                "Complexity 1",
                true,
                "Project Dependency Comment 1",
                true,
                1L, // companyId, created at the top of this method
                null);

        factsheetDTO2 = new FactsheetResponse(
                1L,
                "Factsheet 1",
                "Current Situation 1",
                "Room For Improvement 1",
                "Measure 1",
                "Risks 1",
                "Remarks 1",
                55.0f,
                "Complexity 1",
                true,
                "Project Dependency Comment 1",
                true,
                "Testcompany",
                null);

        // when(companyRepository.findById(1L)).thenReturn(Optional.of(company1));
        // when(companyRepository.findById(2L)).thenReturn(Optional.of(company2));
        // when(factsheetRepository.findAll()).thenReturn(factsheets);
        // when(factsheetRepository.findById(1L)).thenReturn(Optional.of(factsheet1));
        // when(factsheetRepository.findById(2L)).thenReturn(Optional.of(factsheet2));
        // when(factsheetRepository.save(any(Factsheet.class))).thenReturn(factsheet1);

    }

    @Test
    void testSaveFactsheet_Success() {
        // arrange
        when(factsheetRepository.existsByName(any(String.class))).thenReturn(false);
        when(factsheetRepository.save(any(Factsheet.class))).thenReturn(factsheet1);
        when(factsheetMapper.toEntity(any(FactsheetCreateRequest.class))).thenReturn(factsheet1);

        // act
        Factsheet savedFactsheet = factsheetService.saveFactsheet(factsheetDTO1);

        // assert
        assertThat(savedFactsheet).isNotNull();
        assertThat(savedFactsheet.getName()).isEqualTo(factsheetDTO1.getName());
        verify(factsheetRepository, times(1)).save(any(Factsheet.class));

    }

    @Test
    void testSaveFactsheet_DuplicateName() {
        // arrange
        when(factsheetRepository.existsByName(any(String.class))).thenReturn(true);
        when(factsheetMapper.toEntity(any(FactsheetCreateRequest.class))).thenReturn(factsheet1);

        // act and assert
        assertThrows(DuplicateFieldException.class, () -> {
            factsheetService.saveFactsheet(factsheetDTO1);
        });

        verify(factsheetRepository, never()).save(any(Factsheet.class));
        verify(factsheetsavingService, never()).saveFactsheetsavings(any(), any(Factsheet.class));
    }

    @Test
    public void testGetAllFactsheets() {
        // arrange
        when(factsheetRepository.findAll()).thenReturn(factsheets);

        // act
        List<Factsheet> factsheets = factsheetService.getAllFactsheets();

        // assert
        assertNotNull(factsheets);
        assertEquals(factsheets.size(), 2);
        assertEquals(factsheets.get(0).getName(), factsheet1.getName());
        assertEquals(factsheets.get(1).getName(), factsheet2.getName());
    }

    @Test
    public void testGetFactsheetById() {
        // arrange
        when(factsheetRepository.findById(1L)).thenReturn(Optional.of(factsheet1));

        // act
        Factsheet factsheet = factsheetService.getFactsheetById(1L);

        // assert
        assertNotNull(factsheet);
        assertEquals(factsheet.getName(), factsheet1.getName());
    }

    @Test
    public void testUpdateFactsheet() {
        // arrange
        when(factsheetRepository.findById(1L)).thenReturn(Optional.of(factsheet1));
        when(factsheetRepository.save(any(Factsheet.class))).thenReturn(factsheet1);

        when(factsheetMapper.toEntityWithoutSavings(any(FactsheetCreateRequest.class))).thenReturn(factsheet1);
        when(factsheetMapper.toReadDTO(any(Factsheet.class))).thenReturn(factsheetDTO2);
        // act
        FactsheetResponse updatedFactsheet = factsheetService.updateFactsheet(factsheetDTO1, 1L);

        // assert
        assertNotNull(updatedFactsheet);
        assertEquals(updatedFactsheet.getName(), factsheet1.getName());
        assertEquals(updatedFactsheet.getCurrentSituation(), factsheet1.getCurrentSituation());
    }

    @Test
    public void testDeleteFactsheet_success() {
        // arrange
        when(factsheetRepository.existsById(1L)).thenReturn(true);

        // act
        factsheetService.deleteFactsheet(1L);

        // assert
        verify(factsheetRepository, times(1)).deleteById(1L);
    }

    @Test
    public void testDeleteFactsheet_CompanyNotFound() {
        // arrange
        when(factsheetRepository.existsById(1L)).thenReturn(false);

        // act and assert
        assertThrows(ResourceNotFoundException.class, () -> {
            factsheetService.deleteFactsheet(1L);

        });

    }

}