package com.wes.processdb.unit.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import com.wes.processdb.factsheets.dto.FactsheetCreateRequest;
import com.wes.processdb.factsheets.entity.Factsheet;
import com.wes.processdb.factsheets.entity.Factsheetsaving;
import com.wes.processdb.factsheets.repository.FactsheetsavingRepository;
import com.wes.processdb.factsheets.service.FactsheetServiceImpl;
import com.wes.processdb.factsheets.service.FactsheetsavingServiceImpl;
import com.wes.processdb.exception.DuplicateFieldException;
import com.wes.processdb.exception.ResourceNotFoundException;
import com.wes.processdb.company.entity.Company;
import com.wes.processdb.company.repository.CompanyRepository;

import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Optional;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(MockitoExtension.class)
public class FactsheetSavingServiceTests {

    @Mock
    private FactsheetsavingRepository factsheetsavingRepository;


    @InjectMocks
    private FactsheetsavingServiceImpl factsheetsavingService;

  
    private Factsheetsaving factsheetsaving1;
    private Factsheetsaving factsheetsaving2;

    @BeforeEach
    void initMocks() {
        MockitoAnnotations.openMocks(this);


    }


}