package com.wes.processdb;

import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.wes.processdb.exception.DuplicateFieldException;
import com.wes.processdb.exception.ErrorResponse;
import com.wes.processdb.exception.ResourceNotFoundException;
import com.wes.processdb.exception.ValidationException;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Objects;
import java.sql.SQLIntegrityConstraintViolationException;
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(DuplicateFieldException.class)
    protected ResponseEntity<Object> handleDuplicateFieldException(DuplicateFieldException ex) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
        return buildResponseEntity(errorResponse);
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Object> handleValidationException(ValidationException ex) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, ex.getMessage());

        return buildResponseEntity(errorResponse);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
        return buildResponseEntity(errorResponse);
    }

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public ResponseEntity<Object> handleIntegrityConstraintViolationException(
            SQLIntegrityConstraintViolationException ex) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
        return buildResponseEntity(errorResponse);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleResourceNotFoundException(ResourceNotFoundException ex) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    // Create a method to create response entity with error response
    private ResponseEntity<Object> buildResponseEntity(ErrorResponse errorResponse) {
        if (errorResponse != null && errorResponse.getStatusCode() != null) {
            return new ResponseEntity<>(errorResponse, Objects.requireNonNull(errorResponse.getStatusCode()));
        } else {
            
            
            // This is to handle the case where errorResponse or its status code is null
            // You can customize this part based on your logging or exception handling
            System.err.println("Error: Invalid ErrorResponse or StatusCode is null");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal Server Error");
        }
    }
}