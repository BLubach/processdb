package com.wes.processdb.factsheets.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.wes.processdb.exception.ValidationException;
import com.wes.processdb.factsheets.dto.FactsheetCreateRequest;
import com.wes.processdb.factsheets.dto.FactsheetResponse;
import com.wes.processdb.factsheets.dto.FactsheetsavingDTO;
import com.wes.processdb.factsheets.entity.Factsheet;
import com.wes.processdb.factsheets.entity.Factsheetsaving;
import com.wes.processdb.factsheets.mapper.FactsheetMapper;
import com.wes.processdb.factsheets.service.FactsheetService;
import com.wes.processdb.factsheets.service.FactsheetsavingService;

import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.*;
import java.util.stream.Collectors;
import java.util.List;

@RestController
@RequestMapping("/api/v1/factsheets")
public class FactsheetRestController {

    private final FactsheetService factsheetService;
    private final FactsheetsavingService factsheetsavingService;
    private final FactsheetMapper factsheetMapper;

    public FactsheetRestController(FactsheetService factsheetService, FactsheetsavingService factsheetsavingService,
            FactsheetMapper factsheetMapper) {
        this.factsheetService = factsheetService;
        this.factsheetsavingService = factsheetsavingService;
        this.factsheetMapper = factsheetMapper;
    }

    @PostMapping("")
    public ResponseEntity<Object> createFactsheet(@Valid @RequestBody FactsheetCreateRequest factsheetDTO,
            BindingResult result) {
        List<FieldError> errors = result.getFieldErrors();

        if (!errors.isEmpty()) {
            String errorMessage = errors.stream()
                    .map(FieldError::getDefaultMessage)
                    .collect(Collectors.joining(", "));
            throw new ValidationException(errorMessage);
        }

        Factsheet factsheet = factsheetService.saveFactsheet(factsheetDTO);
        return new ResponseEntity<>(factsheetMapper.toReadDTO(factsheet), HttpStatus.CREATED);
    }

    @GetMapping("")
    public ResponseEntity<List<FactsheetResponse>> getAllFactsheets(
            @RequestParam(required = false) Boolean projectDependency,
            @RequestParam(required = false) Boolean influenceOnProduction,
            @RequestParam(required = false) String complexity,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String currentSituation,
            @RequestParam(required = false) String roomForImprovement,
            @RequestParam(required = false) String measure,
            @RequestParam(required = false) String risks,
            @RequestParam(required = false) String remarks,
            @RequestParam(required = false) Float investment) {

        List<Factsheet> factsheets = factsheetService.getFilteredFactsheets(
                name,
                currentSituation,
                roomForImprovement,
                measure,
                risks,
                remarks,
                investment,
                complexity,
                projectDependency,
                influenceOnProduction);

        List<FactsheetResponse> factsheetReadDTOs = factsheetMapper.toReadDTOList(factsheets);

        return new ResponseEntity<>(factsheetReadDTOs, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FactsheetResponse> getFactsheetById(@PathVariable("id") Long id) {

        Factsheet factsheet = factsheetService.getFactsheetById(id);

        return new ResponseEntity<>(factsheetMapper.toReadDTO(factsheet), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<FactsheetResponse> updateFactsheet(@PathVariable("id") Long id, @RequestBody FactsheetCreateRequest factsheetDTO) {
        FactsheetResponse updatedFactsheet = factsheetService.updateFactsheet(factsheetDTO, id);
        return new ResponseEntity<>(updatedFactsheet, HttpStatus.OK);
    }

 

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteFactsheet(@PathVariable("id") Long id) {
        factsheetService.deleteFactsheet(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}/savings")
    public ResponseEntity<FactsheetResponse> getFactsheetWithSavings(@PathVariable Long id) {
        Factsheet factsheet = factsheetService.getFactsheetById(id);

        if (factsheet == null) {
            return ResponseEntity.notFound().build();
        }

        List<Factsheetsaving> factsheetsavings = factsheetsavingService.getFactsheetsavings(factsheet);
        List<FactsheetsavingDTO> factsheetsavingDTOs = factsheetsavingService.convertToDTOList(factsheetsavings);
        FactsheetResponse factsheetReadDTO = factsheetMapper.toReadDTO(factsheet);

        return new ResponseEntity<>(factsheetReadDTO, HttpStatus.OK);
    }

}
