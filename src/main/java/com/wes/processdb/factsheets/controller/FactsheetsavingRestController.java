
package com.wes.processdb.factsheets.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;

import com.wes.processdb.factsheets.dto.FactsheetsavingDTO;
import com.wes.processdb.factsheets.entity.Factsheet;
import com.wes.processdb.factsheets.entity.Factsheetsaving;
import com.wes.processdb.factsheets.service.FactsheetService;
import com.wes.processdb.factsheets.service.FactsheetsavingService;

import java.util.List;


@RestController
@RequestMapping("/api/v1/factsheetsavings")
public class FactsheetsavingRestController {

    @Autowired
    private FactsheetsavingService factsheetsavingService;

    @Autowired
    private FactsheetService factsheetService;

    @GetMapping("/{id}/savings")
    public ResponseEntity<List<FactsheetsavingDTO>> getFactsheetWithSavings(@PathVariable Long id) {
        Factsheet factsheet = factsheetService.getFactsheetById(id);

        if (factsheet == null) {
            return ResponseEntity.notFound().build();
        }

        List<Factsheetsaving> factsheetsavings = factsheetsavingService.getFactsheetsavings(factsheet); 
        List<FactsheetsavingDTO> factsheetsavingDTOs = factsheetsavingService.convertToDTOList(factsheetsavings);

        return new ResponseEntity<>(factsheetsavingDTOs, HttpStatus.OK);
    }
}