package com.wes.processdb.factsheets.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.persistence.CascadeType;
import jakarta.persistence.OneToMany;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wes.processdb.company.entity.Company;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "factsheet")
public class Factsheet {

    public Factsheet(String name, String currentSituation, String roomForImprovement, String measure, String risks,
            String remarks, Float investment, String complexity, Boolean projectDependency,
            String projectDependencyComment, Boolean influenceOnProduction, Company company) {
        this.name = name;
        this.currentSituation = currentSituation;
        this.roomForImprovement = roomForImprovement;
        this.measure = measure;
        this.risks = risks;
        this.remarks = remarks;
        this.investment = investment;
        this.complexity = complexity;
        this.projectDependency = projectDependency;
        this.projectDependencyComment = projectDependencyComment;
        this.influenceOnProduction = influenceOnProduction;
        this.company = company;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "factsheet_id")
    private Long id;

    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "company_id", referencedColumnName = "company_id")
    private Company company;

    @NotBlank(message = "The 'name' field is required.")
    @NotNull
    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "current_situation")
    private String currentSituation;

    @Column(name = "room_for_improvement")
    private String roomForImprovement;

    @Column(name = "measure")
    private String measure;

    @Column(name = "risks")
    private String risks;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "investment")
    private Float investment;

    @Column(name = "complexity")
    private String complexity;

    @Column(name = "project_dependency")
    private Boolean projectDependency;

    @Column(name = "project_dependency_comment")
    private String projectDependencyComment;

    @Column(name = "influence_on_production")
    private Boolean influenceOnProduction;

    @JsonIgnore
    @OneToMany(mappedBy = "factsheet", cascade = CascadeType.ALL)
    private List<Factsheetsaving> factsheetsaving;

}
