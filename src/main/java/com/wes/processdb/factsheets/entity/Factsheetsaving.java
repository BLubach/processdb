package com.wes.processdb.factsheets.entity; 


import com.wes.processdb.utilityuse.entity.Utility;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "factsheet_saving")
@NoArgsConstructor
@Getter
@Setter
public class Factsheetsaving {

    public Factsheetsaving(Factsheet factsheet, Utility utility, Float price, Float amount, String uom) {
        this.factsheet = factsheet;
        this.utility = utility;
        this.price = price;
        this.amount = amount;
        this.uom = uom;
      
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "saving_id")
    private Long id;
        
    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "factsheet_id", referencedColumnName = "factsheet_id")
    private Factsheet factsheet;
  
    @NotNull(message = "The 'utilityID' is needed.")
    @ManyToOne(optional = false)
    @JoinColumn(name = "utility_id", referencedColumnName = "utility_id")
    private Utility utility;

    @NotNull
    @Column(name = "price")
    private Float price;

    @NotNull
    @Column(name = "amount")
    private Float amount;

    @NotNull
    @Column(name = "uom")
    private String uom;


}
