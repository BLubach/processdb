package com.wes.processdb.factsheets.mapper;


import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.wes.processdb.factsheets.dto.FactsheetsavingDTO;
import com.wes.processdb.factsheets.entity.Factsheetsaving;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FactsheetsavingMapper {

    private final ModelMapper modelMapper;

    public FactsheetsavingMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    
    
    public Factsheetsaving toEntity(FactsheetsavingDTO savingDTO) {
        return modelMapper.map(savingDTO, Factsheetsaving.class);
    }

    public FactsheetsavingDTO toDTO(Factsheetsaving saving) {
        return modelMapper.map(saving, FactsheetsavingDTO.class);
    }


    public List<FactsheetsavingDTO> toDTOList(List<Factsheetsaving> savings) {
        return savings.stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    public List<Factsheetsaving> toEntityList(List<FactsheetsavingDTO> savingDTOs) {
        return savingDTOs.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }



}
