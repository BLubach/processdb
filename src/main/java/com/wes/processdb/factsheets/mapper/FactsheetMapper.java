package com.wes.processdb.factsheets.mapper;

import com.wes.processdb.company.entity.Company;
import com.wes.processdb.company.service.CompanyService;
import com.wes.processdb.factsheets.dto.FactsheetCreateRequest;
import com.wes.processdb.factsheets.dto.FactsheetResponse;
import com.wes.processdb.factsheets.dto.FactsheetsavingDTO;
import com.wes.processdb.factsheets.entity.Factsheet;
import com.wes.processdb.factsheets.entity.Factsheetsaving;
import com.wes.processdb.factsheets.service.FactsheetsavingService;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FactsheetMapper {

    private final ModelMapper modelMapper;
    private final FactsheetsavingService factsheetsavingService;
    private final FactsheetsavingMapper factsheetsavingMapper;
    private final CompanyService companyService;

    public FactsheetMapper(ModelMapper modelMapper, FactsheetsavingService factsheetsavingService,
            FactsheetsavingMapper factsheetsavingMapper, CompanyService companyService) {
        this.modelMapper = modelMapper;
        this.factsheetsavingService = factsheetsavingService;
        this.factsheetsavingMapper = factsheetsavingMapper;
        this.companyService = companyService;

    }

    public Factsheet toEntityWithoutSavings(FactsheetCreateRequest createDTO) {
        // Convert FactsheetCreateDTO to Factsheet
        Factsheet factsheet = modelMapper.map(createDTO, Factsheet.class);
        factsheet.setFactsheetsaving(null);

        // Handle Company mapping
        Company company = companyService.getCompany(createDTO.getCompanyId());
        factsheet.setCompany(company);

        return factsheet;
    }

    public Factsheet toEntity(FactsheetCreateRequest createDTO) {
        // Convert FactsheetCreateDTO to Factsheet
        Factsheet factsheet = modelMapper.map(createDTO, Factsheet.class);

        // Handle Company mapping
        Company company = companyService.getCompany(createDTO.getCompanyId());
        factsheet.setCompany(company);

        // Handle nested Factsheetsavings
        if (createDTO.getFactsheetsavingDTO() != null) {
            List<Factsheetsaving> factsheetsavings = createDTO.getFactsheetsavingDTO().stream()
                    .map(dto -> {
                        // Convert FactsheetsavingDTO to Factsheetsaving entity
                        Factsheetsaving factsheetsaving = factsheetsavingMapper.toEntity(dto);

                        // Set the parent Factsheet reference
                        factsheetsaving.setFactsheet(factsheet);

                        return factsheetsaving;
                    })
                    .collect(Collectors.toList());

            // Set the list of Factsheetsavings in the Factsheet
            factsheet.setFactsheetsaving(factsheetsavings);
        }

        return factsheet;
    }

    public List<Factsheet> toEntityList(List<FactsheetCreateRequest> createDTOList) {
        return createDTOList.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }



    
    public FactsheetResponse toReadDTO(Factsheet factsheet) {
        // Use ModelMapper to map basic fields
        FactsheetResponse factsheetReadDTO = modelMapper.map(factsheet, FactsheetResponse.class);

        // Handle complex fields
        List<Factsheetsaving> factsheetsavings = factsheetsavingService.getFactsheetsavings(factsheet);
        List<FactsheetsavingDTO> factsheetsavingDTO = factsheetsavingMapper.toDTOList(factsheetsavings);

        // Set complex fields manually
        factsheetReadDTO.setCompanyName(factsheet.getCompany().getName());
        factsheetReadDTO.setFactsheetsavings(factsheetsavingDTO);

        return factsheetReadDTO;
    }

    public List<FactsheetResponse> toReadDTOList(List<Factsheet> factsheets) {
        return factsheets.stream()
                .map(this::toReadDTO)
                .collect(Collectors.toList());
    }
}
