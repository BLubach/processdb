package com.wes.processdb.factsheets.service;

import com.wes.processdb.exception.ResourceNotFoundException;
import com.wes.processdb.factsheets.dto.FactsheetsavingDTO;
import com.wes.processdb.factsheets.entity.Factsheet;
import com.wes.processdb.factsheets.entity.Factsheetsaving;
import com.wes.processdb.factsheets.repository.FactsheetsavingRepository;
import com.wes.processdb.utilityuse.entity.Utility;
import com.wes.processdb.utilityuse.repository.UtilityRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

import lombok.*;

@AllArgsConstructor
@Service
public class FactsheetsavingServiceImpl implements FactsheetsavingService {

    @Autowired
    private FactsheetsavingRepository factsheetsavingRepository;

    @Autowired
    private UtilityRepository utilityRepository;

    @Override
    public List<Factsheetsaving> saveFactsheetsavings(List<FactsheetsavingDTO> factsheetsavingDTOs, Factsheet factsheet) {
        List<Factsheetsaving> savedFactsheets = new ArrayList<>();
        if (factsheetsavingDTOs != null) {
            for (FactsheetsavingDTO dto : factsheetsavingDTOs) {

                Utility utility = utilityRepository.findById(dto.getUtilityID())
                        .orElseThrow(() -> new ResourceNotFoundException("Utility not found"));

                Factsheetsaving factsheetsaving = new Factsheetsaving(
                        factsheet,
                        utility,
                        dto.getPrice(),
                        dto.getAmount(),
                        dto.getUom());

                savedFactsheets.add(factsheetsavingRepository.save(factsheetsaving));
            }
        }
        return savedFactsheets;
    }

    @Override
    public List<Factsheetsaving> getAllFactsheetsavings() {
        return factsheetsavingRepository.findAll();
    }

    @Override
    public List<Factsheetsaving> getFactsheetsavings(Long factsheetId) {
        return factsheetsavingRepository.findByFactsheetId(factsheetId);
    }

    @Override
    public List<Factsheetsaving> getFactsheetsavings(Factsheet factsheet) {
        return factsheetsavingRepository.findByFactsheet(factsheet);
    }

    @Override
    public List<FactsheetsavingDTO> convertToDTOList(List<Factsheetsaving> savings) {
        return savings.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    public FactsheetsavingDTO convertToDTO(Factsheetsaving saving) {
        return new FactsheetsavingDTO(
                saving.getUtility().getId(),
                saving.getPrice(),
                saving.getAmount(),
                saving.getUom());
    }

    public void deleteFactsheetsaving(Long factsheetId) {

        factsheetsavingRepository.deleteByFactsheetId(factsheetId);
    }

}
