package com.wes.processdb.factsheets.service;

import com.wes.processdb.company.repository.CompanyRepository;
import com.wes.processdb.company.service.CompanyService;
import com.wes.processdb.factsheets.dto.FactsheetCreateRequest;
import com.wes.processdb.factsheets.dto.FactsheetResponse;
import com.wes.processdb.factsheets.entity.Factsheet;
import com.wes.processdb.factsheets.mapper.FactsheetMapper;
import com.wes.processdb.factsheets.mapper.FactsheetsavingMapper;
import com.wes.processdb.factsheets.repository.FactsheetRepository;
import com.wes.processdb.factsheets.repository.FactsheetsavingRepository;
import com.wes.processdb.factsheets.specification.FactsheetSpecification;
import com.wes.processdb.exception.DuplicateFieldException;
import com.wes.processdb.exception.ResourceNotFoundException;

import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import lombok.*;

@AllArgsConstructor
@Service
public class FactsheetServiceImpl implements FactsheetService {

    @Autowired
    private FactsheetRepository factsheetRepository;

    @Autowired
    private FactsheetMapper factsheetMapper;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private FactsheetsavingMapper factsheetsavingMapper;

    @Autowired
    private FactsheetsavingRepository factsheetsavingRepository;

    @Autowired
    private FactsheetsavingService factsheetsavingService;

    // Transactional, the factsheet is saved first, then the nested Factsheetsavings
    // are saved. If there is an error after the factsheet is saved, the factsheet
    // is rolled back.
    @Transactional
    public Factsheet saveFactsheet(FactsheetCreateRequest createDTO) {

        Factsheet factsheet = factsheetMapper.toEntity(createDTO);
        // check if factsheet name already exists
        if (factsheetRepository.existsByName(factsheet.getName())) {
            throw new DuplicateFieldException("Factsheet name already exists");
        }
        // Save the factsheet
        Factsheet savedFactsheet = factsheetRepository.save(factsheet);

        return savedFactsheet;
    }

    public Factsheet getFactsheetById(Long factsheetId) {
        Optional<Factsheet> factsheet = factsheetRepository.findById(factsheetId);

        return unwrapFactsheet(factsheet, factsheetId);

    }

    public List<Factsheet> getAllFactsheets() {
        return (List<Factsheet>) factsheetRepository.findAll();
    }

    public List<Factsheet> getFilteredFactsheets(
            String name,
            String currentSituation,
            String roomForImprovement,
            String measure,
            String risks,
            String remarks,
            Float investment,
            String complexity,
            Boolean projectDependency,
            Boolean influenceOnProduction) {

        final Specification<Factsheet> specification = FactsheetSpecification.filterFactsheet(
                name,
                currentSituation,
                roomForImprovement,
                measure,
                risks,
                remarks,
                investment,
                complexity,
                projectDependency,
                influenceOnProduction);

        final List<Factsheet> factsheets = factsheetRepository.findAll(specification);

        return factsheets;
    }

    @Transactional
    public FactsheetResponse updateFactsheet(FactsheetCreateRequest factsheetDTO, Long id) {

        Factsheet existingFactsheet = unwrapFactsheet(factsheetRepository.findById(id), id);

        // Check if the new name is unique, ignoring the current factsheet
        String newName = factsheetDTO.getName();
        if (isNameTakenByAnotherFactsheet(newName, id)) {
            throw new IllegalArgumentException("A factsheet with the name '" + newName + "' already exists.");
        }

        // // update the factsheet
        existingFactsheet = factsheetMapper.toEntityWithoutSavings(factsheetDTO);
        existingFactsheet.setId(id);

        // remove all existing factsheet savings, this will be replaced by the new ones
        deleteAllSavings(id);

        // add the new factsheet savings
        factsheetsavingService.saveFactsheetsavings(factsheetDTO.getFactsheetsavings(), existingFactsheet);
        factsheetRepository.save(existingFactsheet);

        return factsheetMapper.toReadDTO(existingFactsheet);
    }

    public void deleteFactsheet(Long id) {
        // Check if the factsheet exists before attempting to delete
        if (!factsheetRepository.existsById(id)) {
            throw new ResourceNotFoundException("Factsheet does not exist");
        }
        factsheetRepository.deleteById(id);
    }

    public void deleteAllSavings(Long factsheetId) {

        // Fetch the Factsheet to ensure it exists
        factsheetRepository.findById(factsheetId)
                .orElseThrow(() -> new ResourceNotFoundException("Factsheet does not exist"));

        factsheetsavingRepository.deleteByFactsheetId(factsheetId);

    }

    // helper method to unwrap company from optional
    static Factsheet unwrapFactsheet(Optional<Factsheet> factsheetOptional, Long id) {
        // If the factsheet does not exist, throw an exception
        if (factsheetOptional.isEmpty()) {
            throw new ResourceNotFoundException("Factsheet not found with ID: " + id);
        }
        // Otherwise, return the factsheet
        return factsheetOptional.get();
    }

    private boolean isNameTakenByAnotherFactsheet(String name, Long currentId) {
        // Find factsheet by name
        Optional<Factsheet> optionalFactsheetWithSameName = factsheetRepository.findByName(name);

        // If there is a factsheet with the same name, check if it's not the current one
        return optionalFactsheetWithSameName
                .map(factsheet -> !factsheet.getId().equals(currentId))
                .orElse(false);
    }

}
