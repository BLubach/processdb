package com.wes.processdb.factsheets.service;

import com.wes.processdb.factsheets.dto.FactsheetCreateRequest;
import com.wes.processdb.factsheets.dto.FactsheetResponse;
import com.wes.processdb.factsheets.entity.Factsheet;

import java.util.List;


public interface FactsheetService {

    Factsheet saveFactsheet(FactsheetCreateRequest factsheetDTO);

    List<Factsheet> getAllFactsheets();
    Factsheet getFactsheetById(Long id);
    
    void deleteAllSavings(Long factsheetId);

    FactsheetResponse updateFactsheet(FactsheetCreateRequest factsheetDTO, Long id);
    void deleteFactsheet(Long id);

    List<Factsheet> getFilteredFactsheets(
        String name,
        String currentSituation,
        String roomForImprovement,
        String measure,
        String risks,
        String remarks,
        Float investment,
        String complexity,
        Boolean projectDependency,
        Boolean influenceOnProduction
    );

}
