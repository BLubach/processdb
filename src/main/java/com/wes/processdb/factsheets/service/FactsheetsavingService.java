package com.wes.processdb.factsheets.service;
import com.wes.processdb.factsheets.dto.FactsheetsavingDTO;
import com.wes.processdb.factsheets.entity.Factsheet;
import com.wes.processdb.factsheets.entity.Factsheetsaving;

import java.util.List;

public interface FactsheetsavingService {
    
    List<Factsheetsaving> saveFactsheetsavings(List<FactsheetsavingDTO> factsheetsavingDTOs, Factsheet factsheet);
    List<Factsheetsaving> getAllFactsheetsavings();
    List<Factsheetsaving> getFactsheetsavings(Long factsheetId);
    List<Factsheetsaving> getFactsheetsavings(Factsheet factsheet);
    List<FactsheetsavingDTO> convertToDTOList(List<Factsheetsaving> factsheetsavings);

    void deleteFactsheetsaving(Long factsheetId);

}
