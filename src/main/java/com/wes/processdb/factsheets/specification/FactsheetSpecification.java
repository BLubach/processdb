package com.wes.processdb.factsheets.specification; 


import com.wes.processdb.factsheets.entity.Factsheet;

import org.springframework.data.jpa.domain.Specification;

import jakarta.persistence.criteria.Predicate;


public class FactsheetSpecification {

    public static Specification<Factsheet> filterFactsheet(
            String name,
            String currentSituation,
            String roomForImprovement,
            String measure,
            String risks,
            String remarks,
            Float investment,
            String complexity,
            Boolean projectDependency,
            Boolean influenceOnProduction) {
        return (root, query, criteriaBuilder) -> {
            Predicate predicate = criteriaBuilder.conjunction();
            
            if (name != null && !name.isEmpty()) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("name"), "%" + name + "%"));
            }
            
            if (currentSituation != null && !currentSituation.isEmpty()) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("currentSituation"), "%" + currentSituation + "%"));
            }
            
            if (roomForImprovement != null && !roomForImprovement.isEmpty()) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("roomForImprovement"), "%" + roomForImprovement + "%"));
            }
            
            if (measure != null && !measure.isEmpty()) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("measure"), "%" + measure + "%"));
            }
            
            if (risks != null && !risks.isEmpty()) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("risks"), "%" + risks + "%"));
            }
            
            if (remarks != null && !remarks.isEmpty()) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("remarks"), "%" + remarks + "%"));
            }
            
            if (investment != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("investment"), investment));
            }
            
            if (complexity != null && !complexity.isEmpty()) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("complexity"), "%" + complexity + "%"));
            }
            
            if (projectDependency != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("projectDependency"), projectDependency));
            }
            
            if (influenceOnProduction != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("influenceOnProduction"), influenceOnProduction));
            }
            

            
            return predicate;
        };
    }
}
