package com.wes.processdb.factsheets.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

import jakarta.validation.Valid;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FactsheetCreateRequest {
    private String name;
    private String currentSituation;
    private String roomForImprovement;
    private String measure;
    private String risks;
    private String remarks;
    private Float investment;
    private String complexity;
    private Boolean projectDependency;
    private String projectDependencyComment;
    private Boolean influenceOnProduction;
    private Long companyId;

    @Valid
    private List<FactsheetsavingDTO> factsheetsavings;

    

    public List<FactsheetsavingDTO> getFactsheetsavingDTO() {
        return factsheetsavings;
    }

    public void setFactsheetsavingDTO(List<FactsheetsavingDTO> factsheetsavings) {
        this.factsheetsavings = factsheetsavings;
    }
}
