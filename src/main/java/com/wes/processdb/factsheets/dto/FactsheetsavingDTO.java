package com.wes.processdb.factsheets.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.wes.processdb.factsheets.entity.Factsheetsaving;

import jakarta.validation.constraints.NotNull;


@Getter
@Setter
@NoArgsConstructor
public class FactsheetsavingDTO {
   

    private Long utilityID;
    
    @NotNull(message = "Savings price is required") 
    private Float price;

    @NotNull(message = "Savings amount is required") 
    private Float amount;


    @NotNull(message = "Savings UOM is required") 
    private String uom; 
    

    public FactsheetsavingDTO(Long utilityID, Float price, Float amount, String uom) {
        this.utilityID = utilityID;
        this.price = price;
        this.amount = amount;
        this.uom = uom;
    }

    public FactsheetsavingDTO(Factsheetsaving factsheetsaving){
        this.utilityID = factsheetsaving.getUtility().getId();
        this.price = factsheetsaving.getPrice();
        this.amount = factsheetsaving.getAmount();
        this.uom = factsheetsaving.getUom();
    }
}
