package com.wes.processdb.factsheets.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FactsheetResponse {

    private Long id;
    private String name;
    private String currentSituation;
    private String roomForImprovement;
    private String measure;
    private String risks;
    private String remarks;
    private Float investment;
    private String complexity;
    private Boolean projectDependency;
    private String projectDependencyComment;
    private Boolean influenceOnProduction;
    private String companyName;
    private List<FactsheetsavingDTO> factsheetsavings;


    public List<FactsheetsavingDTO> getFactsheetsavings() {
        return factsheetsavings;
    }

    public void setFactsheetsavings(List<FactsheetsavingDTO> factsheetsavings) {
        this.factsheetsavings = factsheetsavings;
    }
}
