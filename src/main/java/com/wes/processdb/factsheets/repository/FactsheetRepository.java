package com.wes.processdb.factsheets.repository;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

import com.wes.processdb.factsheets.entity.Factsheet;

import java.util.List;
import java.util.Optional;

@Repository
public interface FactsheetRepository extends JpaRepository<Factsheet, Long> {


    List<Factsheet> findAll();

    List<Factsheet> findAll(Specification<Factsheet> specification);

    List<Factsheet> findByInfluenceOnProduction(Boolean influenceOnProduction);

    Optional<Factsheet> findById(Long id);
    
    Optional<Factsheet> findByName(String name);

    Factsheet save(Factsheet factsheet);

    @Transactional
    void deleteById(Long id);

    @Transactional
    void delete(Factsheet factsheet);

    boolean existsById(Long id);

    boolean existsByName(String name);

    long count();
    

}
