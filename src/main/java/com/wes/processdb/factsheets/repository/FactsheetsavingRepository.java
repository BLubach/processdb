package com.wes.processdb.factsheets.repository;
import org.springframework.stereotype.Repository;

import com.wes.processdb.factsheets.entity.Factsheet;
import com.wes.processdb.factsheets.entity.Factsheetsaving;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface FactsheetsavingRepository extends JpaRepository<Factsheetsaving, Long> {

    void deleteByFactsheetId(Long factsheetId);
    

    
    List<Factsheetsaving> findAll();

    Optional<Factsheetsaving> findById(Long id);

    Factsheetsaving save(Factsheetsaving factsheetsaving);
    
    void deleteById(Long id);
    
    void delete(Factsheetsaving factsheetsaving);
    
    boolean existsById(Long id);
    
    long count();

    List<Factsheetsaving> findByFactsheetId(Long factsheetId);

    List<Factsheetsaving> findByFactsheet(Factsheet factsheet);
    
    
    
}
