package com.wes.processdb.company.dto;

import com.wes.processdb.company.entity.Company;

import org.modelmapper.ModelMapper;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDTO {
    
    @NotNull(message = "The 'name' field is required.")
    @Size(min = 1, message = "The 'name' field cannot be empty.")
    private String name;
    private String description;
    private Double latitude;
    private Double longitude;

    private static final ModelMapper modelMapper = new ModelMapper();

    public static CompanyDTO fromEntity(Company company) {
        return modelMapper.map(company, CompanyDTO.class);
    }

    public Company toEntity() {
        return modelMapper.map(this, Company.class);
    }
}
