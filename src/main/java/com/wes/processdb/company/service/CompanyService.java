package com.wes.processdb.company.service;

import com.wes.processdb.company.entity.Company;

import java.util.List;


public interface CompanyService {

    // CRUD operations
    Company saveCompany(Company company);
    // CompanyDTO saveCompany(CompanyDTO companyDTO);
    
    List<Company> getAllCompanies();
    

    Company getCompany(Long companyId);
    Company updateCompany(Long companyId, Company company);
    void deleteCompany(Long companyId);


    Company getOrCreateCompany(String name);

}