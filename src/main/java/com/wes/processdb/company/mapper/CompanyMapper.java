package com.wes.processdb.company.mapper;

import com.wes.processdb.company.dto.CompanyDTO;
import com.wes.processdb.company.entity.Company;

import org.modelmapper.ModelMapper;


public class CompanyMapper {

    private static final ModelMapper companyMapper = new ModelMapper();

    public static CompanyDTO toDTO(Company company) {
        return companyMapper.map(company, CompanyDTO.class);
    }

    public static Company toEntity(CompanyDTO companyDTO) {
        return companyMapper.map(companyDTO, Company.class);
    }
}