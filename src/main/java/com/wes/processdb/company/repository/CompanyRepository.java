package com.wes.processdb.company.repository; 

import com.wes.processdb.company.entity.Company;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Optional;


@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    // Company findByCompanyId(Long companyId);
    Optional<Company> findByName(String name);

    Boolean existsByName(String name);

    
    @Override
    List<Company> findAll();

    @Transactional
    void deleteById(Long companyId);

    

}
