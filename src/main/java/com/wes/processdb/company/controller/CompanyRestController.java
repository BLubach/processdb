package com.wes.processdb.company.controller;

import com.wes.processdb.company.dto.CompanyDTO;
import com.wes.processdb.company.entity.Company;
import com.wes.processdb.company.service.CompanyService;
import com.wes.processdb.exception.ValidationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import java.util.stream.Collectors;

import java.util.List;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class CompanyRestController {

    private final CompanyService companyService;

    public CompanyRestController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @PostMapping("/companies")
    public ResponseEntity<CompanyDTO> createCompany(@Valid @RequestBody CompanyDTO companyDTO, BindingResult result) {
        List<FieldError> errors = result.getFieldErrors();
        if (!errors.isEmpty()) {
            String errorMessage = errors.stream()
                    .map(FieldError::getDefaultMessage)
                    .collect(Collectors.joining(", "));
            throw new ValidationException(errorMessage);
        }

        Company toBeSavedCompany = companyDTO.toEntity();
        Company savedCompany = companyService.saveCompany(toBeSavedCompany);
        CompanyDTO savedCompanyDTO = CompanyDTO.fromEntity(savedCompany);

        return new ResponseEntity<>(savedCompanyDTO, HttpStatus.CREATED);

    }

    @GetMapping("/companies")
    public ResponseEntity<List<CompanyDTO>> getAllCompanies() {

        List<Company> companies = companyService.getAllCompanies();

        List<CompanyDTO> companyDTOs = companies.stream()
                .map(CompanyDTO::fromEntity)
                .collect(Collectors.toList());
        return ResponseEntity.ok(companyDTOs);
    }

    @GetMapping("/companies/{companyId}")
    public ResponseEntity<CompanyDTO> getCompanyById(@PathVariable Long companyId) {

        Company companyToReturn = companyService.getCompany(companyId);
        CompanyDTO companyDTO = CompanyDTO.fromEntity(companyToReturn);

        return ResponseEntity.ok(companyDTO); 
   
    }

    @PutMapping("/companies/{companyId}")
    public ResponseEntity<CompanyDTO> updateCompany(@PathVariable Long companyId, @Valid @RequestBody CompanyDTO company, BindingResult result) {
        List<FieldError> errors = result.getFieldErrors();
        if (!errors.isEmpty()) {
            String errorMessage = errors.stream()
                    .map(FieldError::getDefaultMessage)
                    .collect(Collectors.joining(", "));
            throw new ValidationException(errorMessage);
        }

        Company updatedCompany = companyService.updateCompany(companyId, company.toEntity());
        return new ResponseEntity<>(CompanyDTO.fromEntity(updatedCompany), HttpStatus.OK);
    }

    @DeleteMapping("/companies/{companyId}")
    public ResponseEntity<Void> deleteCompany(@PathVariable Long companyId) {

        companyService.deleteCompany(companyId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
