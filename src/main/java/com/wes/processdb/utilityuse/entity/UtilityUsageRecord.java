package com.wes.processdb.utilityuse.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.wes.processdb.common.Month;
import com.wes.processdb.company.entity.Company;

import java.time.LocalDate;
import jakarta.persistence.EnumType;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "utility_usage_records", uniqueConstraints = @UniqueConstraint(columnNames = { "utility_id", "month",
    "company_id", "usage_year" }))
public class UtilityUsageRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "utility_usage_record_id")
    private Long id;

    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "utility_id", referencedColumnName = "utility_id")
    private Utility utility;

    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "company_id", referencedColumnName = "company_id")
    private Company company;

    @NotNull
    @Column(name = "amount")
    private Double amount;

    @Column(name = "price")
    private Double price;

    @Column(name = "uom")
    private String uom;

    @Enumerated(EnumType.STRING)
    @Column(name = "month")
    private Month month;

    @Column(name = "usage_year")
    private Integer usageYear;

    @Column(name = "entry_date")
    private LocalDate entryDate;

}