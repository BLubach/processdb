package com.wes.processdb.utilityuse.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.wes.processdb.utilityuse.entity.UtilityUsageRecord;

@Repository
public interface UtilityUsageRepository extends JpaRepository<UtilityUsageRecord, Long>, JpaSpecificationExecutor<UtilityUsageRecord> {

    @Override
    List<UtilityUsageRecord> findAll();

    void deleteById(Long utilityId);

    List<UtilityUsageRecord> findByCompanyId(Long companyId);
    
    


}
