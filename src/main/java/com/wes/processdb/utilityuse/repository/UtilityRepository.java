package com.wes.processdb.utilityuse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wes.processdb.utilityuse.entity.Utility;

import java.util.List;

@Repository
public interface UtilityRepository extends JpaRepository<Utility, Long> {

    @Override
    List<Utility> findAll();


    boolean existsByName(String name);

    Utility findByName(String name);

    void deleteById(Long utilityId);
    


}
