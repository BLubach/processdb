package com.wes.processdb.utilityuse.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.wes.processdb.utilityuse.dto.UtilityUsagePostRequestDTO;
import com.wes.processdb.utilityuse.dto.UtilityUsageResponseDTO;
import com.wes.processdb.utilityuse.entity.Utility;
import com.wes.processdb.utilityuse.entity.UtilityUsageRecord;
import com.wes.processdb.utilityuse.repository.UtilityRepository;
import com.wes.processdb.utilityuse.service.UtilityService;
import com.wes.processdb.company.entity.Company;
import com.wes.processdb.company.repository.CompanyRepository;

import jakarta.persistence.EntityNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UtilityUsageMapper {

    private final ModelMapper modelMapper;
    private final UtilityService utilityService;
    private UtilityRepository utilityRepository;
    private CompanyRepository companyRepository;

    public UtilityUsageMapper(ModelMapper modelMapper, UtilityService utilityService, UtilityRepository utilityRepository, CompanyRepository companyRepository) {
        this.modelMapper = modelMapper;
        this.utilityService = utilityService;
        this.utilityRepository = utilityRepository;
        this.companyRepository = companyRepository;

    }

    public UtilityUsageRecord toEntity(UtilityUsagePostRequestDTO dto) {
        UtilityUsageRecord entity = new UtilityUsageRecord();

        // Fetch the Utility and Company entities using their IDs
        Utility utility = utilityRepository.findById(dto.getUtilityId())
                .orElseThrow(() -> new EntityNotFoundException("Utility not found with id " + dto.getUtilityId()));
        Company company = companyRepository.findById(dto.getCompanyId())
                .orElseThrow(() -> new EntityNotFoundException("Company not found with id " + dto.getCompanyId()));

        // Set the fetched entities in the UtilityUsage entity
        entity.setUtility(utility);
        entity.setCompany(company);

        // Set other fields
        entity.setAmount(dto.getAmount());
        entity.setPrice(dto.getPrice());
        entity.setUom(dto.getUom());
        entity.setMonth(dto.getMonth());
        entity.setUsageYear(dto.getYear());

        return entity;
    }

    public UtilityUsageResponseDTO toResponseDTO(UtilityUsageRecord utilityUsage) {
     

        UtilityUsageResponseDTO dto = modelMapper.map(utilityUsage, UtilityUsageResponseDTO.class);
     
        return dto;
    }

    public List<UtilityUsageResponseDTO> toDTOList(List<UtilityUsageRecord> utilityUsages) {

        // Convert to DTOs
        return utilityUsages.stream()
                .map(usage -> toResponseDTO(usage))
                .collect(Collectors.toList());
    }

    public List<UtilityUsageRecord> toEntityList(List<UtilityUsagePostRequestDTO> utilityUsageDTOs) {
        return utilityUsageDTOs.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
