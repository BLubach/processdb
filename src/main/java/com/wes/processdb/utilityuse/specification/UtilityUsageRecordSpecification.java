package com.wes.processdb.utilityuse.specification;

import org.springframework.data.jpa.domain.Specification;

import com.wes.processdb.common.Month;
import com.wes.processdb.utilityuse.entity.UtilityUsageRecord;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.time.LocalDate;

public class UtilityUsageRecordSpecification {

    public static Specification<UtilityUsageRecord> filterUtilityUsageRecord(
            Long utilityId,
            Long companyId,
            Double amount,
            Double price,
            String uom,
            Month month,
            Integer year,
            LocalDate entryDate) {
        return (Root<UtilityUsageRecord> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) -> {
            Predicate predicate = criteriaBuilder.conjunction();

            if (utilityId != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("utility").get("id"), utilityId));
            }

            if (companyId != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("company").get("id"), companyId));
            }

            if (amount != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("amount"), amount));
            }

            if (price != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("price"), price));
            }

            if (uom != null && !uom.isEmpty()) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("uom"), "%" + uom + "%"));
            }

            if (month != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("month"), month));
            }

            if (year != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("year"), year));
            }

            if (entryDate != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("entryDate"), entryDate));
            }

            return predicate;
        };
    }
}