package com.wes.processdb.utilityuse.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wes.processdb.exception.DuplicateFieldException;
import com.wes.processdb.exception.ResourceNotFoundException;
import com.wes.processdb.utilityuse.dto.UtilityDTO;
import com.wes.processdb.utilityuse.entity.Utility;
import com.wes.processdb.utilityuse.repository.UtilityRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UtilityServiceImpl implements UtilityService {

    @Autowired
    private UtilityRepository utilityRepository;


    public Utility saveUtility(Utility utility) {
        if (utilityRepository.existsByName(utility.getName())) {
            throw new DuplicateFieldException("Utilityname already exists");
        }

        utilityRepository.save(utility);
        return utilityRepository.save(utility);
    }

    public UtilityDTO saveUtility(UtilityDTO utilityDTO) {

        Utility utilityToBeSaved = utilityDTO.toEntity();
        if (utilityRepository.existsByName(utilityToBeSaved.getName())) {
            throw new DuplicateFieldException("Utilityname already exists");
        }


        Utility savedUtility = utilityRepository.save(utilityToBeSaved);

        return UtilityDTO.fromEntity(savedUtility);
    }

    public List<Utility> getAllUtilities() {
        return (List<Utility>) utilityRepository.findAll();
    }

    public List<UtilityDTO> getAllUtilitiesDTO() {
        return utilityRepository.findAll().stream()
                .map(UtilityDTO::fromEntity)
                .collect(Collectors.toList());
    }

    public Utility getUtility(Long utilityId) {
        Optional<Utility> utility = utilityRepository.findById(utilityId);
        return unwrapUtility(utility, utilityId);
    }

    public UtilityDTO getUtilityDTO(Long utilityId) {
        return UtilityDTO.fromEntity(getUtility(utilityId));
    }

    public Utility updateUtility(Long utilityId, Utility utility) {
        // get Utility to update
        Utility utilityToUpdate = unwrapUtility(utilityRepository.findById(utilityId), utilityId);

        utilityToUpdate.setName(utility.getName());

        return utilityRepository.save(utilityToUpdate);
    }


    public UtilityDTO updateUtility(Long utilityId, UtilityDTO utilityDTO) {
        // get Utility to update
        Utility utilityToUpdate = unwrapUtility(utilityRepository.findById(utilityId), utilityId);

        // update Utility
        utilityToUpdate = utilityDTO.toEntity();
        Utility updateUtility = utilityRepository.save(utilityToUpdate);

        // return updated Utility as DTO
        return UtilityDTO.fromEntity(updateUtility);
    }


    
    public void deleteUtility(Long utilityId) {
        utilityRepository.deleteById(utilityId);
    }

    static Utility unwrapUtility(Optional<Utility> utility, Long utilityId) {

        // if Utility does not exist, throw exception
        if (utility.isEmpty()) {
            throw new ResourceNotFoundException("Utility not found with id: " + utilityId);
        }

        // otherwise, return Utility
        return utility.get();
    }

}
