package com.wes.processdb.utilityuse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

import com.wes.processdb.common.Month;
import com.wes.processdb.company.repository.CompanyRepository;
import com.wes.processdb.company.service.CompanyService;
import com.wes.processdb.utilityuse.dto.UtilityOverviewResponseDTO;
import com.wes.processdb.utilityuse.dto.UtilityUsagePostRequestDTO;
import com.wes.processdb.utilityuse.dto.UtilityUsageResponseDTO;
import com.wes.processdb.utilityuse.entity.UtilityUsageRecord;
import com.wes.processdb.utilityuse.mapper.UtilityUsageMapper;
import com.wes.processdb.utilityuse.repository.UtilityRepository;
import com.wes.processdb.utilityuse.repository.UtilityUsageRepository;
import com.wes.processdb.utilityuse.specification.UtilityUsageRecordSpecification;
import com.wes.processdb.exception.ResourceNotFoundException;

import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Map;

import org.springframework.stereotype.Service;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UtilityUsageServiceImpl implements UtilityUsageSerivce {

    @Autowired
    private UtilityUsageRepository utilityUsageRepository;

    @Autowired
    private UtilityUsageMapper utilityUsageMapper;

    @Autowired
    private UtilityRepository utilityRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CompanyRepository companyRepository;

    @Transactional
    public UtilityUsageResponseDTO saveUtilityUsage(UtilityUsagePostRequestDTO utilityUsageDTO) {
        // Check if the utility exists
        if (!utilityRepository.existsById(utilityUsageDTO.getUtilityId())) {
            throw new ResourceNotFoundException("Utility not found with ID: " + utilityUsageDTO.getUtilityId());
        }

        if (!companyRepository.existsById(utilityUsageDTO.getCompanyId())) {
            throw new ResourceNotFoundException("Company not found with ID: " + utilityUsageDTO.getCompanyId());
        }

        // Map DTO to entity
        UtilityUsageRecord utilityUsage = utilityUsageMapper.toEntity(utilityUsageDTO);

        // Insert current date as entry date
        utilityUsage.setEntryDate(LocalDate.now());

        // Save the entity
        UtilityUsageRecord savedUtilityUsage = utilityUsageRepository.save(utilityUsage);

        // Convert saved entity to DTO using the utilityNameMap
        return utilityUsageMapper.toResponseDTO(savedUtilityUsage);
    }

    public List<UtilityUsageResponseDTO> getAllUtilityUsagesDTO() {
        return utilityUsageMapper.toDTOList(utilityUsageRepository.findAll());

    }

    public List<UtilityUsageResponseDTO> getFilteredUtilityUsageRecords(
            Long utilityId,
            Long companyId,
            Double amount,
            Double price,
            String uom,
            Month month,
            Integer year,
            LocalDate entryDate) {


        final Specification<UtilityUsageRecord> specification = UtilityUsageRecordSpecification.filterUtilityUsageRecord(
                utilityId,
                companyId,
                amount,
                price,
                uom,
                month,
                year,
                entryDate);

        final List<UtilityUsageRecord> utilityUsageRecords = utilityUsageRepository.findAll(specification);

        return utilityUsageMapper.toDTOList(utilityUsageRecords);

    }

    public UtilityUsageResponseDTO getUtilityUsageDTO(Long utilityUsageId) {
        // Get record, if exists
        UtilityUsageRecord utilityUsageRecord = unwrapUtilityUsageRecord(
                utilityUsageRepository.findById(utilityUsageId), utilityUsageId);

        // Convert the entity to DTO using the utilityNameMap
        return utilityUsageMapper.toResponseDTO(utilityUsageRecord);
    }

    public UtilityUsageResponseDTO updateUtilityUsage(Long utilityUsageId, UtilityUsagePostRequestDTO utilityUsageDTO) {

        UtilityUsageRecord utilityUsageRecord = unwrapUtilityUsageRecord(
                utilityUsageRepository.findById(utilityUsageId), utilityUsageId);

        // check if utility exists
        if (!utilityRepository.existsById(utilityUsageDTO.getUtilityId())) {
            throw new ResourceNotFoundException("Utility not found with ID: " + utilityUsageDTO.getUtilityId());
        }

        // update record
        utilityUsageRecord = utilityUsageMapper.toEntity(utilityUsageDTO);
        utilityUsageRecord.setId(utilityUsageId);

        // Convert the entity to DTO using the utilityNameMap
        return utilityUsageMapper.toResponseDTO(utilityUsageRepository.save(utilityUsageRecord));

    }

    public void deleteUtilityUsage(Long utilityUsageId) {
        // get record, if exists
        UtilityUsageRecord utilityUsageRecord = unwrapUtilityUsageRecord(
                utilityUsageRepository.findById(utilityUsageId), utilityUsageId);

        // delete record
        utilityUsageRepository.delete(utilityUsageRecord);
    }

    public UtilityOverviewResponseDTO getUtilityOverview(Long companyId) {
        // Fetch data from repository
        List<UtilityUsageRecord> utilityData = utilityUsageRepository.findByCompanyId(companyId);

        // Fetch company name
        String comapnyName = companyService.getCompany(companyId).getName();

        // Process data to create summary
        Map<Long, Map<Integer, UtilityOverviewResponseDTO.UtilityOverviewDTO>> summaryMap = new HashMap<>();

        for (UtilityUsageRecord usage : utilityData) {
            Integer year = 2021;
            //usage.getUsageYear();
            Long utilityId = usage.getUtility().getId();
            String utilityName = usage.getUtility().getName();
            Double amount = usage.getAmount();
            Double price = usage.getPrice();

            summaryMap
                    .computeIfAbsent(utilityId, k -> new HashMap<>())
                    .compute(year, (y, overview) -> {
                        if (overview == null) {
                            overview = new UtilityOverviewResponseDTO.UtilityOverviewDTO();
                            overview.setYear(year);
                            overview.setUtilityId(utilityId);
                            overview.setUtilityName(utilityName);
                            overview.setTotal(0.0);
                            overview.setPriceAvg(0.0);
                            overview.setTotalCost(0.0);
                            overview.setPriceMin(Double.MAX_VALUE);
                            overview.setPriceMax(Double.MIN_VALUE);
                        }
                        overview.setTotal(overview.getTotal() + amount);
                        overview.setTotalCost(overview.getTotalCost() + (price * amount)); // Accumulate total price
                        overview.setPriceMin(Math.min(overview.getPriceMin(), price));
                        overview.setPriceMax(Math.max(overview.getPriceMax(), price));
                        return overview;
                    });
        }
        // Calculate weighted average prices for each utility and year
        summaryMap.forEach((utilityId, yearMap) -> yearMap.forEach((year, overview) -> {
            double totalAmount = overview.getTotal();
            double totalPrice = overview.getTotalCost();

            // Calculate weighted average price
            if (totalAmount > 0) {
                overview.setPriceAvg(totalPrice / totalAmount);
            } else {
                overview.setPriceAvg(0.0);
            }
        }));

        // Flatten the map to list
        List<UtilityOverviewResponseDTO.UtilityOverviewDTO> overviewList = summaryMap.values().stream()
                .flatMap(yearMap -> yearMap.values().stream())
                .collect(Collectors.toList());

        // Return the top-level DTO
        return new UtilityOverviewResponseDTO(companyId, comapnyName, overviewList);
    }

    // helper method to unwrap company from optional
    static UtilityUsageRecord unwrapUtilityUsageRecord(Optional<UtilityUsageRecord> recordOptional, Long id) {
        // If the factsheet does not exist, throw an exception
        if (recordOptional.isEmpty()) {
            throw new ResourceNotFoundException("Usage record not found with ID: " + id);
        }
        // Otherwise, return the factsheet
        return recordOptional.get();
    }
}