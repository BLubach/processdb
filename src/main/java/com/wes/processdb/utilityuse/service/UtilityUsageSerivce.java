package com.wes.processdb.utilityuse.service;

import com.wes.processdb.common.Month;
import com.wes.processdb.utilityuse.dto.UtilityOverviewResponseDTO;
import com.wes.processdb.utilityuse.dto.UtilityUsagePostRequestDTO;
import com.wes.processdb.utilityuse.dto.UtilityUsageResponseDTO;

import java.time.LocalDate;
import java.util.List;

public interface UtilityUsageSerivce {

    UtilityUsageResponseDTO saveUtilityUsage(UtilityUsagePostRequestDTO utilityUsageDTO);

    List<UtilityUsageResponseDTO> getAllUtilityUsagesDTO();

    List<UtilityUsageResponseDTO> getFilteredUtilityUsageRecords(
            Long utilityId,
            Long companyId,
            Double amount,
            Double price,
            String uom,
            Month month,
            Integer year,
            LocalDate entryDate);

    UtilityUsageResponseDTO getUtilityUsageDTO(Long utilityUsageId);

    UtilityUsageResponseDTO updateUtilityUsage(Long utilityUsageId, UtilityUsagePostRequestDTO utilityUsageDTO);

    void deleteUtilityUsage(Long utilityUsageId);

    UtilityOverviewResponseDTO getUtilityOverview(Long companyId);

}