package com.wes.processdb.utilityuse.service;

import com.wes.processdb.utilityuse.dto.UtilityDTO;
import com.wes.processdb.utilityuse.entity.Utility;

import java.util.List;

public interface UtilityService {

    // CRUD operations
    Utility saveUtility(Utility utility);
    UtilityDTO saveUtility(UtilityDTO utilityDTO);

    List<Utility> getAllUtilities();
    List<UtilityDTO> getAllUtilitiesDTO();

    Utility getUtility(Long utilityId);
    UtilityDTO getUtilityDTO(Long utilityId);

    Utility updateUtility(Long utilityId, Utility utility);
    UtilityDTO updateUtility(Long utilityId, UtilityDTO utilityDTO);
        
    void deleteUtility(Long utilityId);

}
