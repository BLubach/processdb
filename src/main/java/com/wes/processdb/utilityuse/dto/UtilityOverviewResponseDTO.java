package com.wes.processdb.utilityuse.dto; 

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * DTO for summarizing utility usage data by year and utility.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UtilityOverviewResponseDTO {

    private Long companyId;
    private String companyName;
    private List<UtilityOverviewDTO> data;

    /**
     * DTO for utility overview details.
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UtilityOverviewDTO {

        private Integer year;
        private Long utilityId;
        private String utilityName;
        private Double total;
        private Double priceAvg;
        private Double priceMin;
        private Double priceMax;
        private Double totalCost; 
    }
}