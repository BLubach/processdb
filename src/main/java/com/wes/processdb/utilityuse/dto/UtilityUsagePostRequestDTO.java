package com.wes.processdb.utilityuse.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.wes.processdb.common.Month;

import jakarta.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UtilityUsagePostRequestDTO {

    @NotNull(message = "The 'utilityId' field is required.")
    private Long utilityId;

    @NotNull(message = "The 'companyId' field is required.")
    private Long companyId; 
    private Double amount;
    private Double price;
    private String uom;
    private Month month;
    private Integer year;

}
