package com.wes.processdb.utilityuse.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.modelmapper.ModelMapper;

import com.wes.processdb.utilityuse.entity.Utility;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UtilityDTO {

    @NotNull(message = "The 'name' field is required.")
    @Size(min = 1, message = "The 'name' field cannot be empty.")
    private String name;

    private static final ModelMapper modelMapper = new ModelMapper();

    public static UtilityDTO fromEntity(Utility utility) {
        return modelMapper.map(utility, UtilityDTO.class);
    }

        public Utility toEntity() {
        return modelMapper.map(this, Utility.class);
    }
}