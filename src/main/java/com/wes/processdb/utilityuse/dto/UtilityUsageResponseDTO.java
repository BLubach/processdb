package com.wes.processdb.utilityuse.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.wes.processdb.common.Month;

import java.time.LocalDate;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class UtilityUsageResponseDTO {


    private Long utilityId; 
    private Long companyId; 
    private String utilityName; 
    private Double amount;
    private Double price;
    private String uom;
    private LocalDate entryDate;
    private Month month;


}
