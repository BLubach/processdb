package com.wes.processdb.utilityuse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.wes.processdb.utilityuse.dto.UtilityOverviewResponseDTO;
import com.wes.processdb.utilityuse.dto.UtilityUsagePostRequestDTO;
import com.wes.processdb.utilityuse.dto.UtilityUsageResponseDTO;
import com.wes.processdb.utilityuse.service.UtilityUsageServiceImpl;
import com.wes.processdb.common.Month;
import com.wes.processdb.exception.ValidationException;

import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.*;
import java.util.stream.Collectors;
import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/api/v1/utilityusage")
public class UtilityUsageRecordRestController {

    @Autowired
    private UtilityUsageServiceImpl utilityUsageService;

    @PostMapping("")
    public ResponseEntity<Object> createUtilityUsage(@Valid @RequestBody UtilityUsagePostRequestDTO utilityUsageDTO,
            BindingResult result) {
        List<FieldError> errors = result.getFieldErrors();

        if (!errors.isEmpty()) {
            String errorMessage = errors.stream()
                    .map(FieldError::getDefaultMessage)
                    .collect(Collectors.joining(", "));
            throw new ValidationException(errorMessage);
        }

        UtilityUsageResponseDTO utilityUsageRecord = utilityUsageService.saveUtilityUsage(utilityUsageDTO);
        return new ResponseEntity<>(utilityUsageRecord, HttpStatus.CREATED);
    }

    @GetMapping("")
    public ResponseEntity<List<UtilityUsageResponseDTO>> getUtilityUsageRecords(
            @RequestParam(required = false) Long utilityId,
            @RequestParam(required = false) Long companyId,
            @RequestParam(required = false) Double amount,
            @RequestParam(required = false) Double price,
            @RequestParam(required = false) String uom,
            @RequestParam(required = false) Month month,
            @RequestParam(required = false) Integer year,
            @RequestParam(required = false) LocalDate entryDate) {

        List<UtilityUsageResponseDTO> utilityUsageRecord = utilityUsageService.getFilteredUtilityUsageRecords(
                utilityId,
                companyId,
                amount,
                price,
                uom,
                month,
                year,
                entryDate);

        return new ResponseEntity<>(utilityUsageRecord, HttpStatus.OK);
    }

    @GetMapping("/{utilityUsageId}")
    public ResponseEntity<UtilityUsageResponseDTO> getUtilityUsage(@PathVariable Long utilityUsageId) {
        return new ResponseEntity<>(utilityUsageService.getUtilityUsageDTO(utilityUsageId), HttpStatus.OK);
    }

    @PutMapping("/{utilityUsageId}")
    public ResponseEntity<UtilityUsageResponseDTO> updateUtilityUsage(@PathVariable Long utilityUsageId,
            @Valid @RequestBody UtilityUsagePostRequestDTO utilityUsageDTO) {
        return new ResponseEntity<>(utilityUsageService.updateUtilityUsage(utilityUsageId, utilityUsageDTO),
                HttpStatus.OK);
    }

    @DeleteMapping("/{utilityUsageId}")
    public ResponseEntity<Object> deleteUtilityUsage(@PathVariable Long utilityUsageId) {
        utilityUsageService.deleteUtilityUsage(utilityUsageId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/overview/{companyId}")
    public ResponseEntity<UtilityOverviewResponseDTO> getUtilityOverview(@PathVariable Long companyId) {
        return new ResponseEntity<>(utilityUsageService.getUtilityOverview(companyId), HttpStatus.OK);
    }

}
