package com.wes.processdb.utilityuse.controller;
import com.wes.processdb.exception.ValidationException;
import com.wes.processdb.utilityuse.dto.UtilityDTO;
import com.wes.processdb.utilityuse.service.UtilityService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import java.util.stream.Collectors;

import java.util.List;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class UtilityRestController {

    private final UtilityService utilityService;
  

    public UtilityRestController(UtilityService utilityService) {
        this.utilityService = utilityService;    
    }

    @PostMapping("/utilities")
    public ResponseEntity<?> createUtility(@Valid @RequestBody UtilityDTO utilityDTO, BindingResult result) {
        List<FieldError> errors = result.getFieldErrors();
        if (!errors.isEmpty()) {
            String errorMessage = errors.stream()
                    .map(FieldError::getDefaultMessage)
                    .collect(Collectors.joining(", "));
            throw new ValidationException(errorMessage);
        }

        return new ResponseEntity<>(utilityService.saveUtility(utilityDTO), HttpStatus.CREATED);
   
    }

    @GetMapping("/utilities")
    public ResponseEntity<List<UtilityDTO>> getAllUtilities() {
        return new ResponseEntity<>(utilityService.getAllUtilitiesDTO(), HttpStatus.OK);
    }

    @GetMapping("/utilities/{utilityId}")
    public ResponseEntity<UtilityDTO> getUtilityById(@PathVariable Long utilityId) {
        UtilityDTO utilityToReturn = utilityService.getUtilityDTO(utilityId);

        return new ResponseEntity<>(utilityToReturn, HttpStatus.OK);
    }

    @PutMapping("/utilities/{utilityId}")
    public ResponseEntity<UtilityDTO> updateUtility(@PathVariable Long utilityId,
            @RequestBody UtilityDTO utilityDTO) {
        return new ResponseEntity<>(utilityService.updateUtility(utilityId, utilityDTO), HttpStatus.OK);
    }

    @DeleteMapping("/utilities/{utilityId}")
    public ResponseEntity<Object> deleteUtility(@PathVariable Long utilityId) {
        utilityService.deleteUtility(utilityId);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
}
